
motorbikeEdit.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
BWheel
  rotate: true
  xy: 2, 524
  size: 123, 124
  orig: 123, 124
  offset: 0, 0
  index: -1
BikeBody
  rotate: false
  xy: 2, 771
  size: 527, 251
  orig: 527, 251
  offset: 0, 0
  index: -1
BikeHandle
  rotate: false
  xy: 747, 578
  size: 161, 157
  orig: 161, 157
  offset: 0, 0
  index: -1
BikePArt1
  rotate: true
  xy: 421, 558
  size: 47, 104
  orig: 47, 104
  offset: 0, 0
  index: -1
BikePart3
  rotate: false
  xy: 531, 944
  size: 215, 78
  orig: 215, 78
  offset: 0, 0
  index: -1
BikePrt
  rotate: true
  xy: 703, 712
  size: 111, 42
  orig: 111, 42
  offset: 0, 0
  index: -1
FBlush
  rotate: true
  xy: 128, 581
  size: 66, 26
  orig: 66, 26
  offset: 0, 0
  index: -1
FBrow
  rotate: true
  xy: 690, 721
  size: 27, 10
  orig: 27, 10
  offset: 0, 0
  index: -1
FFace
  rotate: false
  xy: 741, 470
  size: 133, 106
  orig: 133, 106
  offset: 0, 0
  index: -1
FNeck
  rotate: true
  xy: 703, 863
  size: 38, 43
  orig: 38, 43
  offset: 0, 0
  index: -1
FR_Arm2
  rotate: true
  xy: 673, 605
  size: 67, 72
  orig: 67, 72
  offset: 0, 0
  index: -1
FR_Hand2
  rotate: true
  xy: 702, 674
  size: 36, 42
  orig: 36, 42
  offset: 0, 0
  index: -1
FR_Shoulder2
  rotate: true
  xy: 931, 801
  size: 80, 86
  orig: 80, 86
  offset: 0, 0
  index: -1
FR_Sleeve2
  rotate: false
  xy: 673, 540
  size: 66, 63
  orig: 66, 63
  offset: 0, 0
  index: -1
FWheel
  rotate: true
  xy: 295, 538
  size: 123, 124
  orig: 123, 124
  offset: 0, 0
  index: -1
Fdress sleeve left copy
  rotate: false
  xy: 498, 444
  size: 61, 61
  orig: 61, 61
  offset: 0, 0
  index: -1
Fdress up
  rotate: true
  xy: 748, 878
  size: 144, 181
  orig: 144, 181
  offset: 0, 0
  index: -1
Femalehair
  rotate: false
  xy: 531, 809
  size: 170, 133
  orig: 170, 133
  offset: 0, 0
  index: -1
Feyebrow copy
  rotate: true
  xy: 687, 676
  size: 19, 13
  orig: 19, 13
  offset: 0, 0
  index: -1
Fhair
  rotate: true
  xy: 530, 584
  size: 128, 141
  orig: 128, 141
  offset: 0, 0
  index: -1
Fhead_mouth
  rotate: true
  xy: 673, 674
  size: 21, 12
  orig: 21, 12
  offset: 0, 0
  index: -1
Fhead_nose
  rotate: true
  xy: 498, 525
  size: 17, 23
  orig: 17, 23
  offset: 0, 0
  index: -1
Fleft_arm copy
  rotate: true
  xy: 931, 933
  size: 42, 90
  orig: 42, 90
  offset: 0, 0
  index: -1
Fleft_hand copy
  rotate: true
  xy: 613, 479
  size: 35, 46
  orig: 35, 46
  offset: 0, 0
  index: -1
Fleft_leg up
  rotate: true
  xy: 913, 646
  size: 153, 109
  orig: 153, 109
  offset: 0, 0
  index: -1
Fleft_leg up copy
  rotate: false
  xy: 160, 663
  size: 149, 106
  orig: 149, 106
  offset: 0, 0
  index: -1
Fleft_shoe
  rotate: false
  xy: 421, 495
  size: 75, 61
  orig: 75, 61
  offset: 0, 0
  index: -1
Fleft_shoe copy
  rotate: false
  xy: 665, 479
  size: 73, 59
  orig: 73, 59
  offset: 0, 0
  index: -1
Fleft_sholder copy
  rotate: true
  xy: 2, 410
  size: 61, 92
  orig: 61, 92
  offset: 0, 0
  index: -1
Fleg1
  rotate: true
  xy: 910, 573
  size: 71, 108
  orig: 71, 108
  offset: 0, 0
  index: -1
Fleg1 copy
  rotate: false
  xy: 459, 663
  size: 69, 106
  orig: 69, 106
  offset: 0, 0
  index: -1
Fright eye ball copy 2
  rotate: true
  xy: 913, 802
  size: 14, 13
  orig: 14, 13
  offset: 0, 0
  index: -1
Fright eye ball copy 3
  rotate: false
  xy: 498, 511
  size: 13, 12
  orig: 13, 12
  offset: 0, 0
  index: -1
Fright eye down copy 2
  rotate: false
  xy: 498, 544
  size: 24, 12
  orig: 24, 12
  offset: 0, 0
  index: -1
Fright eye down copy 3
  rotate: true
  xy: 690, 750
  size: 23, 11
  orig: 23, 11
  offset: 0, 0
  index: -1
Fright eye up copy 2
  rotate: true
  xy: 913, 848
  size: 28, 16
  orig: 28, 16
  offset: 0, 0
  index: -1
Fright eye up copy 3
  rotate: false
  xy: 128, 565
  size: 26, 14
  orig: 26, 14
  offset: 0, 0
  index: -1
Fright eye white copy 2
  rotate: false
  xy: 876, 514
  size: 30, 30
  orig: 30, 30
  offset: 0, 0
  index: -1
Fright eye white copy 3
  rotate: false
  xy: 876, 485
  size: 29, 27
  orig: 29, 27
  offset: 0, 0
  index: -1
MHairBack
  rotate: true
  xy: 703, 825
  size: 36, 43
  orig: 36, 43
  offset: 0, 0
  index: -1
MLayer 9 copy
  rotate: false
  xy: 531, 714
  size: 157, 93
  orig: 157, 93
  offset: 0, 0
  index: -1
Mbody_pelvis
  rotate: true
  xy: 909, 439
  size: 132, 107
  orig: 132, 107
  offset: 0, 0
  index: -1
Mbody_torso
  rotate: true
  xy: 748, 737
  size: 139, 163
  orig: 139, 163
  offset: 0, 0
  index: -1
Mhead_face
  rotate: false
  xy: 160, 555
  size: 133, 106
  orig: 133, 106
  offset: 0, 0
  index: -1
Mhead_mouth
  rotate: true
  xy: 690, 775
  size: 32, 11
  orig: 32, 11
  offset: 0, 0
  index: -1
Mhead_neck
  rotate: true
  xy: 703, 903
  size: 39, 43
  orig: 39, 43
  offset: 0, 0
  index: -1
Mhead_nose
  rotate: true
  xy: 128, 541
  size: 22, 25
  orig: 22, 25
  offset: 0, 0
  index: -1
Mleft eye ball copy
  rotate: true
  xy: 513, 510
  size: 13, 12
  orig: 13, 12
  offset: 0, 0
  index: -1
Mleft eye down copy
  rotate: true
  xy: 690, 697
  size: 22, 10
  orig: 22, 10
  offset: 0, 0
  index: -1
Mleft eye up copy
  rotate: false
  xy: 128, 511
  size: 24, 14
  orig: 24, 14
  offset: 0, 0
  index: -1
Mleft eye white copy
  rotate: false
  xy: 876, 457
  size: 27, 26
  orig: 27, 26
  offset: 0, 0
  index: -1
Mleft_leg
  rotate: false
  xy: 2, 649
  size: 156, 120
  orig: 156, 120
  offset: 0, 0
  index: -1
Mleft_leg copy
  rotate: true
  xy: 421, 607
  size: 54, 107
  orig: 54, 107
  offset: 0, 0
  index: -1
Mright eye ball
  rotate: false
  xy: 673, 699
  size: 14, 13
  orig: 14, 13
  offset: 0, 0
  index: -1
Mright eye down
  rotate: false
  xy: 128, 527
  size: 25, 12
  orig: 25, 12
  offset: 0, 0
  index: -1
Mright eye up
  rotate: true
  xy: 913, 818
  size: 28, 16
  orig: 28, 16
  offset: 0, 0
  index: -1
Mright eye white
  rotate: false
  xy: 876, 546
  size: 31, 30
  orig: 31, 30
  offset: 0, 0
  index: -1
Mright_arm
  rotate: false
  xy: 931, 977
  size: 90, 45
  orig: 90, 45
  offset: 0, 0
  index: -1
Mright_arm copy
  rotate: false
  xy: 931, 883
  size: 87, 48
  orig: 87, 48
  offset: 0, 0
  index: -1
Mright_leg
  rotate: false
  xy: 311, 663
  size: 146, 106
  orig: 146, 106
  offset: 0, 0
  index: -1
Mright_leg copy
  rotate: true
  xy: 155, 488
  size: 65, 102
  orig: 65, 102
  offset: 0, 0
  index: -1
Mright_plam
  rotate: true
  xy: 259, 509
  size: 44, 34
  orig: 44, 34
  offset: 0, 0
  index: -1
Mright_shoe
  rotate: true
  xy: 613, 516
  size: 66, 50
  orig: 66, 50
  offset: 0, 0
  index: -1
Mright_shoe copy
  rotate: true
  xy: 99, 443
  size: 66, 51
  orig: 66, 51
  offset: 0, 0
  index: -1
Mright_sholder
  rotate: false
  xy: 527, 507
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
Mright_sholder copy
  rotate: true
  xy: 2, 473
  size: 49, 95
  orig: 49, 95
  offset: 0, 0
  index: -1
right_plam copy
  rotate: false
  xy: 561, 469
  size: 44, 36
  orig: 44, 36
  offset: 0, 0
  index: -1
