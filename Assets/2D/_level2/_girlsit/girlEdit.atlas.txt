
girlEdit.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
Girl/Dress
  rotate: false
  xy: 2, 1567
  size: 421, 479
  orig: 421, 479
  offset: 0, 0
  index: -1
Girl/Face
  rotate: true
  xy: 402, 462
  size: 260, 207
  orig: 260, 207
  offset: 0, 0
  index: -1
Girl/HairBack
  rotate: true
  xy: 135, 816
  size: 333, 260
  orig: 333, 260
  offset: 0, 0
  index: -1
Girl/HairFront
  rotate: false
  xy: 150, 446
  size: 250, 276
  orig: 250, 276
  offset: 0, 0
  index: -1
Girl/L_Arm
  rotate: true
  xy: 135, 724
  size: 90, 172
  orig: 90, 172
  offset: 0, 0
  index: -1
Girl/L_EyeWhite
  rotate: false
  xy: 611, 462
  size: 54, 55
  orig: 54, 55
  offset: 0, 0
  index: -1
Girl/L_Eyeball
  rotate: true
  xy: 779, 13
  size: 24, 23
  orig: 24, 23
  offset: 0, 0
  index: -1
Girl/L_Eyebrow
  rotate: true
  xy: 621, 776
  size: 52, 18
  orig: 52, 18
  offset: 0, 0
  index: -1
Girl/L_EyelidDown
  rotate: false
  xy: 575, 830
  size: 43, 19
  orig: 43, 19
  offset: 0, 0
  index: -1
Girl/L_EyelidUP
  rotate: false
  xy: 622, 303
  size: 50, 27
  orig: 50, 27
  offset: 0, 0
  index: -1
Girl/L_Hand
  rotate: false
  xy: 611, 519
  size: 68, 87
  orig: 68, 87
  offset: 0, 0
  index: -1
Girl/L_LegUP
  rotate: false
  xy: 2, 1138
  size: 131, 427
  orig: 131, 427
  offset: 0, 0
  index: -1
Girl/L_Shoe
  rotate: true
  xy: 150, 298
  size: 146, 118
  orig: 146, 118
  offset: 0, 0
  index: -1
Girl/L_Shoulder
  rotate: true
  xy: 561, 332
  size: 128, 174
  orig: 128, 174
  offset: 0, 0
  index: -1
Girl/L_Sleeve
  rotate: false
  xy: 573, 851
  size: 117, 118
  orig: 117, 118
  offset: 0, 0
  index: -1
Girl/Mouth
  rotate: false
  xy: 397, 816
  size: 38, 23
  orig: 38, 23
  offset: 0, 0
  index: -1
Girl/Neck
  rotate: false
  xy: 730, 647
  size: 73, 82
  orig: 73, 82
  offset: 0, 0
  index: -1
Girl/Nose
  rotate: true
  xy: 660, 4
  size: 33, 44
  orig: 33, 44
  offset: 0, 0
  index: -1
Girl/R_Arm
  rotate: true
  xy: 283, 1152
  size: 78, 174
  orig: 78, 174
  offset: 0, 0
  index: -1
Girl/R_EyeWhite
  rotate: true
  xy: 692, 889
  size: 57, 56
  orig: 57, 56
  offset: 0, 0
  index: -1
Girl/R_Eyeball
  rotate: true
  xy: 726, 304
  size: 26, 25
  orig: 26, 25
  offset: 0, 0
  index: -1
Girl/R_Eyebrow
  rotate: true
  xy: 610, 2
  size: 35, 23
  orig: 35, 23
  offset: 0, 0
  index: -1
Girl/R_EyelidDown
  rotate: false
  xy: 477, 818
  size: 47, 21
  orig: 47, 21
  offset: 0, 0
  index: -1
Girl/R_EyelidUP
  rotate: false
  xy: 510, 299
  size: 54, 29
  orig: 54, 29
  offset: 0, 0
  index: -1
Girl/R_Hand
  rotate: false
  xy: 483, 728
  size: 67, 88
  orig: 67, 88
  offset: 0, 0
  index: -1
Girl/R_LegUP
  rotate: false
  xy: 2, 709
  size: 131, 427
  orig: 131, 427
  offset: 0, 0
  index: -1
Girl/R_Shoe
  rotate: true
  xy: 270, 298
  size: 146, 118
  orig: 146, 118
  offset: 0, 0
  index: -1
Girl/R_Shoulder
  rotate: false
  xy: 397, 971
  size: 116, 179
  orig: 116, 179
  offset: 0, 0
  index: -1
Girl/R_Sleeve
  rotate: false
  xy: 633, 1032
  size: 117, 118
  orig: 117, 118
  offset: 0, 0
  index: -1
PinkGirl/Blush
  rotate: true
  xy: 510, 330
  size: 130, 49
  orig: 130, 49
  offset: 0, 0
  index: -1
PinkGirl/Dress
  rotate: true
  xy: 2, 9
  size: 282, 354
  orig: 282, 354
  offset: 0, 0
  index: -1
PinkGirl/Face
  rotate: true
  xy: 610, 39
  size: 260, 207
  orig: 260, 207
  offset: 0, 0
  index: -1
PinkGirl/HairBack
  rotate: true
  xy: 283, 1232
  size: 333, 260
  orig: 333, 260
  offset: 0, 0
  index: -1
PinkGirl/HairFront
  rotate: false
  xy: 358, 20
  size: 250, 276
  orig: 250, 276
  offset: 0, 0
  index: -1
PinkGirl/L_EyeWhite
  rotate: true
  xy: 667, 463
  size: 54, 52
  orig: 54, 52
  offset: 0, 0
  index: -1
PinkGirl/L_Eyeball
  rotate: true
  xy: 804, 13
  size: 24, 23
  orig: 24, 23
  offset: 0, 0
  index: -1
PinkGirl/L_Eyebrow
  rotate: true
  xy: 641, 776
  size: 52, 18
  orig: 52, 18
  offset: 0, 0
  index: -1
PinkGirl/L_EyelidDown
  rotate: false
  xy: 620, 830
  size: 43, 19
  orig: 43, 19
  offset: 0, 0
  index: -1
PinkGirl/L_EyelidUP
  rotate: false
  xy: 674, 303
  size: 50, 27
  orig: 50, 27
  offset: 0, 0
  index: -1
PinkGirl/L_Hand
  rotate: false
  xy: 681, 519
  size: 68, 87
  orig: 68, 87
  offset: 0, 0
  index: -1
PinkGirl/L_LegUP
  rotate: false
  xy: 2, 293
  size: 146, 414
  orig: 146, 414
  offset: 0, 0
  index: -1
PinkGirl/L_Shoe
  rotate: true
  xy: 390, 298
  size: 146, 118
  orig: 146, 118
  offset: 0, 0
  index: -1
PinkGirl/R_Shoe
  rotate: true
  xy: 390, 298
  size: 146, 118
  orig: 146, 118
  offset: 0, 0
  index: -1
PinkGirl/L_Shoulder
  rotate: true
  xy: 397, 841
  size: 128, 174
  orig: 128, 174
  offset: 0, 0
  index: -1
PinkGirl/L_Sleeve
  rotate: false
  xy: 611, 608
  size: 117, 118
  orig: 117, 118
  offset: 0, 0
  index: -1
PinkGirl/Mouth
  rotate: false
  xy: 437, 816
  size: 38, 23
  orig: 38, 23
  offset: 0, 0
  index: -1
PinkGirl/Neck
  rotate: false
  xy: 692, 948
  size: 73, 82
  orig: 73, 82
  offset: 0, 0
  index: -1
PinkGirl/Nose
  rotate: true
  xy: 706, 4
  size: 33, 44
  orig: 33, 44
  offset: 0, 0
  index: -1
PinkGirl/R_Arm
  rotate: true
  xy: 459, 1152
  size: 78, 174
  orig: 78, 174
  offset: 0, 0
  index: -1
PinkGirl/R_EyeWhite
  rotate: true
  xy: 633, 972
  size: 58, 57
  orig: 58, 57
  offset: 0, 0
  index: -1
PinkGirl/R_Eyeball
  rotate: true
  xy: 752, 11
  size: 26, 25
  orig: 26, 25
  offset: 0, 0
  index: -1
PinkGirl/R_Eyebrow
  rotate: true
  xy: 635, 2
  size: 35, 23
  orig: 35, 23
  offset: 0, 0
  index: -1
PinkGirl/R_EyelidDown
  rotate: false
  xy: 526, 818
  size: 47, 21
  orig: 47, 21
  offset: 0, 0
  index: -1
PinkGirl/R_EyelidUP
  rotate: false
  xy: 566, 301
  size: 54, 29
  orig: 54, 29
  offset: 0, 0
  index: -1
PinkGirl/R_Hand
  rotate: false
  xy: 552, 728
  size: 67, 88
  orig: 67, 88
  offset: 0, 0
  index: -1
PinkGirl/R_LegUP
  rotate: false
  xy: 135, 1151
  size: 146, 414
  orig: 146, 414
  offset: 0, 0
  index: -1
PinkGirl/R_Shoulder
  rotate: false
  xy: 515, 971
  size: 116, 179
  orig: 116, 179
  offset: 0, 0
  index: -1
PinkGirl/R_Sleeve
  rotate: false
  xy: 665, 731
  size: 117, 118
  orig: 117, 118
  offset: 0, 0
  index: -1
PinkGirl/right_arm
  rotate: true
  xy: 309, 724
  size: 90, 172
  orig: 90, 172
  offset: 0, 0
  index: -1
