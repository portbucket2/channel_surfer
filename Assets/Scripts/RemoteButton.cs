using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemoteButton : MonoBehaviour
{
    public int id;
    public Button button;

    public RemoteDirection direction;

    private void Awake()
    {
        button = GetComponent<Button>();
    }

    private void Start()
    {
        button.onClick.AddListener(delegate
        {
            TVManager.Instance.DeactivateAllTV();
            TVManager.Instance.ActivateTv(id);

            UIManager.Instance.RemoteRightButtonPressed(direction);
        });
    }
}


public enum RemoteDirection
{
    RIGHT,
    LEFT
}
