using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPanel : MonoBehaviour
{
    public GameObject holder;

    public List<SelectedItem> selectedItemList;


    public void SetSelectedItem(int id)
    {
        for (int i = 0; i < selectedItemList.Count; i++)
        {
            if(selectedItemList[i].id == id)
            {
                selectedItemList[i].itemObject.SetActive(true);
            }
        }

        ActivateHolder();
    }

    public void ActivateHolder()
    {
        holder.SetActive(true);
    }
    public void DeactivateHolder()
    {
        holder.SetActive(false);
    }

    public void RemoveSelectedItem(int id)
    {
        for (int i = 0; i < selectedItemList.Count; i++)
        {
            if (selectedItemList[i].id == id)
            {
                
            }
        }
    }
}

[System.Serializable]
public struct SelectedItem
{
    public string name;
    public int id;
    public GameObject itemObject;
}