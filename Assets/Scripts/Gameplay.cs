using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    public RayInput rayInput;

    public TVManager tVManager;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }

    private IEnumerator StartGameRoutine()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("------GAMe STARTED -------");

        LionAnalyticsGM.Instance.LevelStarted(LevelManager.Instance.GetCurrentLevel());

        yield return new WaitForSeconds(0.5f);
        rayInput.gameObject.SetActive(true);
        tVManager.ActivateTVManager();
        UIManager.Instance.ActivateRemotePanel();
        tVManager.ActivateTv(0);
    }


   
    public void LoadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GotoNextLevel()
    {
        if (LevelManager.Instance.GetCurrentLevel() > 3)
        {
            PlayerPrefs.DeleteKey("Game_Level_Pref");
        }

        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        string loadScene = "Level" + t_CurrentLevel;

        if (SceneManager.GetActiveScene().name != loadScene)
        {
            SceneManager.LoadScene("Level" + t_CurrentLevel);
        }
    }

    public void LoadAgain()
    {
        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        Debug.Log(t_CurrentLevel + " Scene");
        string loadScene = "Level" + t_CurrentLevel;

        SceneManager.LoadScene("Level" + t_CurrentLevel);

    }
}
