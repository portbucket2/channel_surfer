// Sample written for for Spine 3.7
using UnityEngine;
using Spine;
using Spine.Unity;
using UnityEngine.Events;

public class SpineEventHandler : MonoBehaviour
{
    public UnityEvent OnAnimationStartEvent;
    public UnityEvent OnAnimationCompletedEvent;

    void Start()
    {
        var skeletonAnimation = GetComponent<SkeletonAnimation>();
        if (skeletonAnimation == null) return;

        skeletonAnimation.AnimationState.Start += delegate (TrackEntry trackEntry)
        {
            OnAnimationStartEvent.Invoke();
            Debug.Log(string.Format("track {0} started a new animation.", trackEntry.TrackIndex));
        };

        skeletonAnimation.AnimationState.Complete += delegate
        {
            OnAnimationCompletedEvent.Invoke();
            Debug.Log("An animation ended!");
        };
    }
  
}