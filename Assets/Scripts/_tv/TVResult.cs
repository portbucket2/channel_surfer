using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class TVResult : TVBase
{
    [Header("Item3")]
    public SkeletonAnimation item3SkeletonAnimation;
    public AnimationReferenceAsset item3Idle;

    public AnimationReferenceAsset manLoop;
    public AnimationReferenceAsset thugFearIdle;

    public AnimationReferenceAsset tankLoop;

    public Sprite replacedSprite;
    public SpriteRenderer bgRender;
    public GameObject objectHolder;


    private void Start()
    {
        if (isAuto)
        {
            ActivateThugAnimation(item1Idle, false, 1f);
            ActivateManAnimation(item2Idle, false, 1f);
            ActivateTankAnimation(item3Idle, false, 1f);
        }
        else
        {
            StartCoroutine(WaitAndRun());
        }
    }

    private IEnumerator WaitAndRun()
    {
        yield return new WaitForSeconds(0.3f);
        bgRender.sprite = replacedSprite;
        yield return new WaitForSeconds(0.3f);
        objectHolder.SetActive(false);
    }

    public void PlayThugFearLoop()
    {
        ActivateThugAnimation(thugFearIdle, true, 1f);
    }
    public void PlayManLoop()
    {
        ActivateManAnimation(manLoop, true, 1f);
    }

    public void PlayTankLoop()
    {
        ActivateTankAnimation(tankLoop, true, 1f);
    }

    public void ActivateTankAnimation(AnimationReferenceAsset reference, bool loop, float timeScale)
    {
        item3SkeletonAnimation.state.SetAnimation(0, reference, loop).TimeScale = timeScale;
    }
}
