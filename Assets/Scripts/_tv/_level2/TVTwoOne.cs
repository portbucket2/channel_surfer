using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVTwoOne : TVBase
{
    public List<Animator> animators;


    private void Start()
    {
        StartCoroutine(WaitAndMove());
    }

    private IEnumerator WaitAndMove()
    {
        for (int i = 0; i < animators.Count; i++)
        {
            animators[i].enabled = true;
            yield return new WaitForSeconds(0.3f);
        }
    }
}
