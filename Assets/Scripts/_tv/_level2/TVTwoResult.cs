using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class TVTwoResult : TVBase
{
    public Sprite replacedSprite;
    public SpriteRenderer bgRender;
    public GameObject objectHolder;

    private void Start()
    {
        StartCoroutine(WaitAndRun());
    }

    private IEnumerator WaitAndRun()
    {
        yield return new WaitForSeconds(0.3f);
        bgRender.sprite = replacedSprite;
        yield return new WaitForSeconds(0.3f);
        objectHolder.SetActive(false);
    }
}
