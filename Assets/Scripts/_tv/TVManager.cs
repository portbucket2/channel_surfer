using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVManager : MonoBehaviour
{
    public static TVManager Instance;

    public int currentSelectedTV;

    public GameObject noiseTV;

    public List<TVBase> tvList;

    public GameObject tvScreen;
    public GameObject remoteObject;



    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void ActivateTVManager()
    {
        tvScreen.SetActive(true);
        remoteObject.SetActive(true);
    }
    public void DeactivateTVManager()
    {
        tvScreen.SetActive(false);
        remoteObject.SetActive(false);
    }

    public void DeactivateAllTV()
    {
        for (int i = 0; i < tvList.Count; i++)
        {
            tvList[i].Deactivate();
        }
    }

    public void ActivateTv(int id)
    {

        for (int i = 0; i < tvList.Count; i++)
        {
            if(tvList[i].id == id)
            {
                tvList[i].Activate();
                currentSelectedTV = id;
            }
        }
    }

    public void ReplaceSelectedItem()
    {
        TVBase tv = null;

        for (int i = 0; i < tvList.Count; i++)
        {
            if (tvList[i].id == currentSelectedTV)
            {
                tv = tvList[i];
            }
        }
       // tv.ReplaceItem();
       // UIManager.Instance.RemoveSelectedItem(tv.selectItemId);

        //temp
        StartCoroutine(WaitAndComplete(tv));

        UIManager.Instance.CompleteTutorial();
    }



    public void SelectedItem(GameObject gameObject)
    {
        TVBase tv = null;

        for (int i = 0; i < tvList.Count; i++)
        {
            if (tvList[i].id == currentSelectedTV)
            {
                tv = tvList[i];
            }
        }

        tv.DeactivateSelectItem();

        UIManager.Instance.ActivateSelectedItem(tv.selectItemId);
    }

    IEnumerator WaitAndComplete(TVBase tv)
    {
        DeactivateAllTV();
        noiseTV.SetActive(true);
        yield return new WaitForSeconds(.75f);
        ActivateTv(2);
        noiseTV.SetActive(false);
        yield return new WaitForSeconds(2f);
        UIManager.Instance.ShowLevelComplete();
    }
}
