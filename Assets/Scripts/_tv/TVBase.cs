using System;
using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;

public class TVBase : MonoBehaviour
{
    public bool isAuto;

    [Header("Screen Animation")]
    [Header("Item1")]
    public SkeletonAnimation item1SkeletonAnimation;
    public AnimationReferenceAsset item1Idle;

    [Header("Item2")]
    public SkeletonAnimation item2SkeletonAnimation;
    public AnimationReferenceAsset item2Idle;

    [Space(20)]
    public int id;
    public int selectItemId;
    public GameObject selectItem;
    public GameObject replacedObject;

    private void Start()
    {
        //ActivateThugAnimation(item1Idle, true, 1f);
        //ActivateManAnimation(item2Idle, true, 1f);
    }


    public void ActivateThugAnimation(AnimationReferenceAsset reference, bool loop, float timeScale)
    {
        item1SkeletonAnimation.state.SetAnimation(0, reference, loop).TimeScale = timeScale;
    }
    public void ActivateManAnimation(AnimationReferenceAsset reference, bool loop, float timeScale)
    {
        item2SkeletonAnimation.state.SetAnimation(0, reference, loop).TimeScale = timeScale;
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }
    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void DeactivateSelectItem()
    {
        selectItem.SetActive(false);
    }
    public void ActivateSelectItem()
    {
        selectItem.SetActive(true);
    }

    public void ReplaceItem()
    {
        DeactivateSelectItem();
        replacedObject.SetActive(true);
    }
}
