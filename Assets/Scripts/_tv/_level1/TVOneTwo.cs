using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVOneTwo : TVBase
{
    private void Start()
    {
        ActivateThugAnimation(item1Idle, true, 1f);
        ActivateManAnimation(item2Idle, true, 1f);
    }
}
