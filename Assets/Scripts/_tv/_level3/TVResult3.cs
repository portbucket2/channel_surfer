using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class TVResult3 : TVBase
{
    [Header("Item3")]
    public SkeletonAnimation item3SkeletonAnimation;
    public AnimationReferenceAsset item3Idle;

    private void Start()
    {
      StartCoroutine(WaitAndRun());
    }

    private IEnumerator WaitAndRun()
    {
       // yield return new WaitForSeconds(0.5f);
       // ActivateThugAnimation(item1Idle, false, .9f);
        ActivateManAnimation(item2Idle, false, 1f);
        yield return new WaitForSeconds(0.5f);
        ActivateItem3Animation(item3Idle, false, 1f);
    }

    public void ActivateItem3Animation(AnimationReferenceAsset reference, bool loop, float timeScale)
    {
        item3SkeletonAnimation.state.SetAnimation(0, reference, loop).TimeScale = timeScale;
    }
}
