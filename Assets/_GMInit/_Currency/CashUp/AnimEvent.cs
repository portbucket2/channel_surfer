using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimEvent : MonoBehaviour
{
    public UnityEvent OnAnimationEvent;

    public void AnimEventCalled()
    {
        OnAnimationEvent.Invoke();
    }
}
