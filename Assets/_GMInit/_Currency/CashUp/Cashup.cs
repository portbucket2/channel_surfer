using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cashup : MonoBehaviour
{
    public static Cashup Instance;

    public Animator animator;
    public Text cashText;
    public GameObject root;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        
    }

    private void Start()
    {
        //animator = GetComponent<Animator>();
    }

    public void ActivateCashUp(int cash)
    {
        cashText.text = cash.ToString();
        root.SetActive(true);
    }
    public void DeactivateCashup()
    {
        root.SetActive(false);
    }
}
