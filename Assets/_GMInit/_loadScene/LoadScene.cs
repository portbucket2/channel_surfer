using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f);
        LoadNextScene();
    }

    public void LoadNextScene()
    {
        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        GotoNextLevel();
    }
    public void GotoNextLevel()
    {
        if (LevelManager.Instance.GetCurrentLevel() > 3)
        {
            PlayerPrefs.DeleteKey("Game_Level_Pref");
        }

        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        string loadScene = "Level" + t_CurrentLevel;

        if (SceneManager.GetActiveScene().name != loadScene)
        {
            SceneManager.LoadScene("Level" + t_CurrentLevel);
        }
    }
}
