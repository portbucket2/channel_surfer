
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class JoystickRoaming : MonoBehaviour
{
    public DynamicJoystick dynamicJoystick;

    public GameObject playerObject;

    [Range(0f, 100f)] public float movementSpeed;
    [Range(0f, 100f)] public float playerMovement;


    private bool m_IsTouchedWithWall;

    public static JoystickRoaming Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }


    private void FixedUpdate()
    {
        Vector3 direction = Vector3.forward * dynamicJoystick.Vertical + Vector3.right * dynamicJoystick.Horizontal;

        Vector3 t_ModifiedDirection = direction * movementSpeed * Time.fixedDeltaTime;

        if (t_ModifiedDirection.magnitude > 0)
        {
            playerObject.transform.eulerAngles = new Vector3(0, Mathf.Atan2(dynamicJoystick.Horizontal / 3f, dynamicJoystick.Vertical / 3f) * 180 / Mathf.PI, 0);
        }
        else
        {

        }

        playerObject.GetComponent<Rigidbody>().velocity = t_ModifiedDirection * playerMovement;

    }


    public void ActivateDynamicJoystick()
    {
        dynamicJoystick.gameObject.SetActive(true);
    }

    public void DeactivateDynamicJoystick()
    {
        dynamicJoystick.gameObject.SetActive(false);
    }

    public void SetPlayerPos(Vector3 t_Pos)
    {
        playerObject.transform.position = t_Pos;
    }

    public Transform GetPlayerTrans()
    {
        return playerObject.transform;
    }

    public void SetPlayerLocalPos(Vector3 zero, Transform t_HandTrans)
    {
        StartCoroutine(PlayerMoveToHandRoutine(zero, t_HandTrans));
    }

    IEnumerator PlayerMoveToHandRoutine(Vector3 t_Pos, Transform t_HandTrans)
    {
        while (true)
        {
            playerObject.transform.localPosition = Vector3.Lerp(playerObject.transform.localPosition, t_Pos, 4 * Time.deltaTime);

            if (Vector3.Distance(playerObject.transform.localPosition, t_HandTrans.position) < .01f)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
