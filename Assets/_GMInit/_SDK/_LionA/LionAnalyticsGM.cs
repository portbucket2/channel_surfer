using System.Collections;
using System.Collections.Generic;
using LionStudios.Suite.Analytics;
using LionStudios.Suite.Debugging;
using UnityEngine;

public class LionAnalyticsGM : MonoBehaviour
{
    public static LionAnalyticsGM Instance;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        LionDebugger.Hide();
        LionAnalytics.GameStart();
    }

    public void LevelStarted(int levelNumber)
    {
        LionAnalytics.LevelStart(levelNumber,0);
    }
    public void LevelCompleted(int levelNumber)
    {
        LionAnalytics.LevelComplete(levelNumber,0);
    }
    public void LevelFailed(int levelNumber)
    {
        LionAnalytics.LevelFail(levelNumber, 0);
    }


}
